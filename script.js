window.onload = function() {
	document.getElementById("article_name").value = "Main Page";
	displayArticle();
	document.getElementById("article_name").value = "";
}

function getArticleJSON(page, lang) {
	var baseURL = "https://" + lang + ".wikipedia.org";
	var requestURL = baseURL + "/w/api.php?page=" + page +
					"&action=parse&format=json&prop=text%7Cheadhtml&disabletoc=1&formatversion=2&redirects=1&disableeditsection=1&origin=*";
	
	var xhr = new XMLHttpRequest();
	xhr.open("GET", requestURL, false);
	xhr.send();
	
	return JSON.parse(xhr.responseText);
}

function displayArticle() {
	var userInput = document.getElementById("article_name").value;
	var selectedLang = document.getElementById("article_lang").value;
	
	var article_title = document.getElementById("article_title");
	var article = document.getElementById("article");
	
	article_title.innerHTML = "Fetching...";
	article.innerHTML = "";
	
	var obj = getArticleJSON(userInput, selectedLang);
	
	if (obj.error) {
		article_title.innerHTML = "<h2>Article not found :(</h2>";
		article.innerHTML = "Article titled <b>'" + userInput + "' </b>not found in the selected language of wikipeia.";
		return false;
	}
	
	var title = obj.parse.title;
	var text = obj.parse.text;
	var head = obj.parse.headhtml;
	
	var baseURL = "https://" + selectedLang + ".wikipedia.org";
	document.getElementById("head").innerHTML += "<base href='" + baseURL + "' target='_blank'>" + head;
	
	article.innerHTML = text;
	article_title.innerHTML = "<h1>" + title + "</h1>";
	
	return false;
}

function showSidebar() {
    document.getElementsByClassName("sidebar")[0].style.display = "block";
}
function hideSidebar() {
    document.getElementsByClassName("sidebar")[0].style.display = "none";
}
